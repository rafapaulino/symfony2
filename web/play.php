<?php
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Debug\Debug;
use Rafa\CatalogoBundle\Entity\Catalogo;

$loader = require_once __DIR__.'/../app/bootstrap.php.cache';
Debug::enable();

require_once __DIR__.'/../app/AppKernel.php';

$kernel = new AppKernel('dev', true);
$kernel->loadClassCache();
$kernel->boot();

$container = $kernel->getContainer();
$container->enterScope('request');
$container->set('request', Request::createFromGlobals());

// Working basically with Doctrine
$catalogo = new Catalogo();
$catalogo->setName('Jogos PS4');
$catalogo->setDescricao('Jogos de Playstation 4');
$catalogo->setLancamento(new \DateTime('tomorrow noon'));
$catalogo->setImageName('ps4-games.jpg');

$manager = $container->get('doctrine')->getManager();
$manager->persist($catalogo);
$manager->flush();


$catalogo = new Catalogo();
$catalogo->setName('Jogos Xbox One');
$catalogo->setDescricao('Jogos de Xbox One 4');
$catalogo->setLancamento(new \DateTime('tomorrow noon'));
$catalogo->setImageName('ps4-games.jpg');


$manager = $container->get('doctrine')->getManager();
$manager->persist($catalogo);
$manager->flush();
