CREATE DATABASE  IF NOT EXISTS `symfony` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `symfony`;
-- MySQL dump 10.13  Distrib 5.6.24, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: symfony
-- ------------------------------------------------------
-- Server version	5.6.24-0ubuntu2.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wg_user`
--

DROP TABLE IF EXISTS `wg_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wg_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `is_active` tinyint(1) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wg_user`
--

LOCK TABLES `wg_user` WRITE;
/*!40000 ALTER TABLE `wg_user` DISABLE KEYS */;
INSERT INTO `wg_user` VALUES (1,'user','oQeGwKLVkf63BzG5CXITbOyJSJa02gZTcK+55GNRmIHweiJ4eCC1G3hU6Qr+Zi/5He9mizoXruwh5Wgt23VbOg==','mdfa5whzfz4gco48c0cooc0cks4oco0','a:0:{}',1,'user@son.com'),(2,'admin','01moyGOFm6VuwEEGvSd+tjmfeGvWNfyP46y2dfjG78NsMLbwhelgpPz9tTe7X1BG9X6U7PWZIa+X3WwWlRxeIQ==','o6h5qxdmgqsg04sok8g08ww0440c4cw','a:1:{i:0;s:10:\"ROLE_ADMIN\";}',1,'admin@son.com'),(3,'batman','xMCoFuSeW7OZQYqib5k1K2IbfV7Ua8/vNUBFG02PQULW0VI3n+NzylKNTOOIVE+EanN2iLs12oDMdWLJ4lsxRA==','pq3hwbzthz44ccgo8kg84s00oggcggo','a:0:{}',1,'rafaclasses@gmail.com'),(4,'ninja','sRc0SwvEfOSQENKQ53ubZbInkQj1+j3sId1xUHUqq8CZQc+TD9yKPEiAMJfIOovY92gpgeBoDswaTa8LCsvILg==','6l1mplisg8sgssokkwggo08kgoscwk4','a:0:{}',1,'ninja@uol.com.br');
/*!40000 ALTER TABLE `wg_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-04 22:10:42
