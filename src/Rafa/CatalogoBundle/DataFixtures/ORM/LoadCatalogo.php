<?php
namespace Rafa\CatalogoBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Rafa\CatalogoBundle\Entity\Catalogo;

class LoadCatalogoData implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $catalogo = new Catalogo();
        $catalogo->setName('Jogos PS4');
        $catalogo->setDescricao('Jogos de Playstation 4');
        $catalogo->setLancamento(new \DateTime('tomorrow noon'));
        $catalogo->setImageName('ps4-games.jpg');
        $manager->persist($catalogo);
        $manager->flush();

        $catalogo = new Catalogo();
        $catalogo->setName('Jogos Xbox One');
        $catalogo->setDescricao('Jogos de Xbox One 4');
        $catalogo->setLancamento(new \DateTime('tomorrow noon'));
        $catalogo->setImageName('ps4-games.jpg');

        $manager->persist($catalogo);
        $manager->flush();
    }
}