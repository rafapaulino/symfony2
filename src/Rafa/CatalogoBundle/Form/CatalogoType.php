<?php

namespace Rafa\CatalogoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CatalogoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array('required' => true, 'label' => 'Nome:'))
            ->add('descricao', 'textarea', array('required' => true, 'label' => 'Descrição:'))
            ->add('lancamento', null, array('required' => true, 'label' => 'Lançamento:'))
            ->add('imageName', 'text', array('required' => true, 'label' => 'Nome da imagem:'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Rafa\CatalogoBundle\Entity\Catalogo'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'rafa_catalogobundle_catalogo';
    }
}
