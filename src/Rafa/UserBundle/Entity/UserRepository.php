<?php

namespace Rafa\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;


class UserRepository extends EntityRepository implements UserProviderInterface
{
    public function findOneByUsernameOrEmail($user)
    {
        return $this->createQueryBuilder('u')
            ->where('u.username = :username or u.email = :email')
            ->setParameter("username", $user)
            ->setParameter("email", $user)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function loadUserByUsername($username)
    {
        $user = $this->findOneByUsernameOrEmail($username);

        if (! $user) {
            throw new UsernameNotFoundException("Usuário não encontrado!");
        }

        return $user;
    }

    public function refreshUser(UserInterface $user)
    {
        if (! get_class($user)) {
            throw new UnsupportedUserException("An error has occurred");
        }
        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class)
    {
        return $this->getEntityName() == $class || is_subclass_of($class, $this->getEntityName());
    }
}

