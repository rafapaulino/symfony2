<?php

namespace Rafa\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Rafa\UserBundle\Entity\User;
use Rafa\UserBundle\Form\RegisterFormType;
use Symfony\Component\HttpFoundation\Request;

class RegisterController extends Controller
{
    public function registerAction(Request $request)
    {
        // Recebe a própria entidade
        $form = $this->createForm(new RegisterFormType(), new User());

        if ($request->isMethod('POST')) {
            $form->bind($request);

            if ($form->isValid()) {
                $user = $form->getData();
                $user->setPassword($this->encodePassword($user, $user->getPlainPassword()));

                $this->getDoctrine()->getManager()->persist($user);
                $this->getDoctrine()->getManager()->flush();

                $request->getSession()->getFlashBag()->add('notice', 'Registro feito com sucesso!');

                return $this->redirect($this->generateUrl('catalogo'));
            }
        }

        return $this->render('UserBundle:Register:register.html.twig', array("form"=>$form->createView()));
    }

    private function encodePassword($user, $plainPassword) {
        $encoder = $this->container->get("security.encoder_factory")
            ->getEncoder($user);

        return $encoder->encodePassword($plainPassword, $user->getSalt());
    }
}
