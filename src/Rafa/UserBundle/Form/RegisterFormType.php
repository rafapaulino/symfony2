<?php

namespace Rafa\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class RegisterFormType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder->add("username", 'text', array('required' => true, 'label' => 'Nome de usuário:'))
            ->add("email", "email", array('required' => true, 'label' => 'E-mail:'))
            ->add("plainPassword", "repeated", array(
                'type' => 'password'
            ))
        ;
    }

    public function getDefaultOptions(array $options) {

        return array(
            'data_class' => 'Rafa\UserBundle\Entity\User'
        );
    }

    public function getName() {
        return "user_register";
    }

}